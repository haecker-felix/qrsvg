/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/* pimp-print.vala
 *
 * Copyright (C) 2017 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using GSvg;
using Qrencode;
/**
 * Main class to construct SVG representation of QR codes.
 */
public class Qrs.QR : Object {
  private GSvg.Document _svg;
  private int _width;
  private double _svg_width;

  /**
   * QR version number to use
   */
  public int version { get; set; default = 1; }
  /**
   * Number of units per side in square
   */
  public int width { get { return _width; } }
  /**
   * Width of square in milimeters
   */
  public double square_width { get; set; default = 1.0; }
  /**
   * Width of QRcode in milimeters
   */
  public double svg_width { get { return _svg_width; } }
  /**
   * Generated SVG
   */
  public GSvg.Document svg { get { return _svg; } }
  construct {
    _svg = new GSvg.GsDocument ();
    _width = (version - 1) * 4 + 21;
    square_width = 0.5;
    _svg_width = square_width * width;
    var strw = "%fmm".printf (svg_width);
    var r = svg.add_svg ("0mm", "0mm", strw, strw, null);
  }
  /**
   * Encoding routing
   *
   * @param str string to encode
   * @param quality Error detection quality
   * @param mode QR character mode
   * @param sensitive case sensitive
   */
  public void encode (string str, EcQuality quality, CharacterMode mode, CaseSensitive sensitive) throws GLib.Error
  {
    EcLevel ecl = (EcLevel) quality;
    var qr = new QRcode.encodeString (str, version, (Qrencode.EcLevel) quality, (Qrencode.Mode) mode, sensitive);
    _width = qr.width;
    int x, y;
    x = y = 0;
    for (int i = 0; i < (width * width); i++) {
      uint8 px = qr.data[i];
      string xs = "%fmm".printf (square_width*x);
      string ys = "%fmm".printf (square_width*y);
      x++;
      if (x == qr.width) {
        x = 0;
        y++;
      }
      var tsw = "%fmm".printf (square_width);
      var r = svg.root_element.create_rect (xs, ys, tsw, tsw, null, null);
      if (!(Data.ENABLE in px))
        r.fill = "none";
      svg.root_element.append_child (r);
    }
  }
  public enum EcQuality {
      L,
      M,
      Q,
      H
  }
  public enum CharacterMode {
      NUL,
      NUM,
      AN,
      B8,
      KANJI,
      STRUCTURE
  }
  [Flags]
  public enum Data {
    ENABLE,
    ECC_MODE,
    FORMAT,
    VERSION,
    TIMING,
    ALIGMENT,
    FINDER,
    NON_DATA
  }
  public enum CaseSensitive {
    NO = 0,
    YES = 1
  }
}
